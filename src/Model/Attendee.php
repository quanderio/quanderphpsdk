<?php

namespace Quander\Sdk\Model;

/**
 * @method void setTokens(array $tokens)
 * @method void setAsync(bool $async)
 * @method string getEmail
 * @method string getFirstname
 * @method string getLastname
 * @method string getFields
 * @method string getQrCode
 * @method string getMedia
 * @method string getUuid
 * @method \DateTime getCreatedDate
 */
class Attendee extends Resource
{
    public static function createFromJson($json)
    {
        $data = json_decode($json, true);

        $attendee = new self(
            $data['email'],
            $data['firstname'],
            $data['lastname']
        );

        $attendee->setCreatedDate(\DateTime::createFromFormat(DATE_ISO8601, $data['created_date']));

        $attendee->values['uuid'] = $data['uuid'];

        $attendee->values['uploadDate'] = \DateTime::createFromFormat(DATE_ISO8601, $data['upload_date']);

        $attendee->values['tokens'] = isset($data['tokens']) ? $data['tokens'] : null;

        if (isset($data['qr_code'])) {
            $attendee->values['qrCode'] = $data['qr_code'];
        }

        if (isset($data['fields'])) {
            $attendee->values['fields'] = $data['fields'];
        }

        if (isset($data['media'])) {
            $attendee->values['media'] = $data['media'];
        }

        return $attendee;
    }

    public function __construct(
        $email,
        $firstname,
        $lastname,
        $tokens = null,
        $fields = null,
        $media = null,
        $createdDate = null
    ) {
        $this->values = [
            'email' => $email,
            'firstname' => $firstname,
            'lastname' => $lastname,
        ];

        $createdDate = $createdDate ?: \DateTime::createFromFormat('U', time());

        $this->setCreatedDate($createdDate);

        if ($tokens) {
            $this->values['tokens'] = $fields;
        }

        if ($fields) {
            $this->values['fields'] = $fields;
        }

        if ($media) {
            $this->values['media'] = $media;
        }
    }

    public function setCreatedDate(\DateTime $dateTime)
    {
        $this->values['createdDate'] = $dateTime->format('Y-m-d H:i:s');
    }
}
