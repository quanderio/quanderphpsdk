<?php

namespace Quander\Sdk\Model;

abstract class Resource implements \JsonSerializable
{
    protected $values;

    public function __get($name)
    {
        return isset($this->values[$name]) ? $this->values[$name] : null;
    }

    public function __call($name, $arg)
    {
        if (0 === strpos($name, 'get')) {
            $varname = lcfirst(substr($name, 3));

            return isset($this->values[$varname]) ? $this->values[$varname] : null;
        } elseif (0 === strpos($name, 'set')) {
            $varname = lcfirst(substr($name, 3));

            return $this->values[$varname] = $arg[0];
        } else {
            throw new \Exception('Bad method.', 500);
        }
    }

    public function jsonSerialize()
    {
        return $this->values;
    }
}
