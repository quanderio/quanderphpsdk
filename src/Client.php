<?php

namespace Quander\Sdk;

use GuzzleHttp\Psr7\Request;
use League\OAuth2\Client\Provider\GenericProvider;
use League\OAuth2\Client\Token\AccessToken;
use Quander\Sdk\Model\Resource;

class Client
{
    /**
     * @var GenericProvider
     */
    private $provider;

    private $baseUrl;

    /**
     * @var AccessToken
     */
    private $currentAccessToken;

    public function __construct(
        $clientId,
        $clientSecret,
        $baseUrl = 'http://testing.quander.io/api',
        $tokenUrl = 'http://testing.quander.io'
    ) {
        $this->baseUrl = $baseUrl;

        $this->provider = new GenericProvider([
            'clientId' => $clientId,
            'clientSecret' => $clientSecret,
            'urlAuthorize' => '',
            'urlAccessToken' => $tokenUrl.'/oauth/v2/token',
            'urlResourceOwnerDetails' => '',
        ]);
    }

    public function getAccessTokenWithPassword($username, $password)
    {
        $this->currentAccessToken = $this->provider->getAccessToken('password', [
            'username' => $username,
            'password' => $password,
        ]);

        return $this->currentAccessToken->getRefreshToken();
    }

    public function setAccessToken(AccessToken $existingAccessToken)
    {
        if ($existingAccessToken->hasExpired()) {
            throw new TokenExpiredException();
        }
        $this->currentAccessToken = $existingAccessToken;
    }

    /**
     * @param AccessToken $existingAccessToken
     *
     * @return AccessToken
     */
    public function getRefreshedToken(AccessToken $existingAccessToken)
    {
        return $this->provider->getAccessToken('refresh_token', [
            'refresh_token' => $existingAccessToken->getRefreshToken(),
        ]);
    }

    public function post(Resource $resource, $resourcePath)
    {
        $request = new Request(
            'POST',
            $this->baseUrl.$resourcePath,
            [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer '.$this->currentAccessToken->getToken(),
            ],
            json_encode($resource)
        );

        return $this->provider->getHttpClient()->send($request);
    }

    public function createAttendeeManager($options)
    {
        return new AttendeeManager($this, $options);
    }
}
