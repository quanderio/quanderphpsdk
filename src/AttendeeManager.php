<?php

namespace Quander\Sdk;

use Quander\Sdk\Model\Attendee;

class AttendeeManager
{
    const RESOURCE_PATH = [
        'project' => '/projects/%s/attendees',
    ];

    private $resourcePath;

    private $client;

    public function __construct(Client $client, $options = [])
    {
        $this->client = $client;

        if (isset($options['project'])) {
            $this->resourcePath = sprintf(self::RESOURCE_PATH['project'], $options['project']);
        }
    }

    public function create(Attendee $attendee, $options)
    {
        if (isset($options['async'])) {
            $attendee->setAsync((bool) $options['async']);
        }

        $response = $this->client->post($attendee, $this->resourcePath);

        return Attendee::createFromJson((string) $response->getBody());
    }
}
