# QUANDER PHP SDK

## Installation

```sh
composer require quander/php-sdk
```

## Usage
To write an app using the SDK

* Register for a developer account and get your client_id and secret.
* Add dependency 'quander/php-sdk' in your composer.json file.
* Use 'quander/php-sdk' in your file

```php
use Quander\Sdk\Client as QuanderClient;
```
* Create config options, with parameters (mode, client_id, secret).

```php
$quanderClient = new QuanderClient(
    '3_195mrfc5wfms0wc884ocwkskkk8ogkgsk8kogw8ckcwwwk804s', // clientId
    '4225r5v6y1wk0kks4so0sso4g8ogss44k0kwg4k8co040oks84', // clientSecret
    'http://testing.quander.io/api', // baseUrl
    'http://testing.quander.io' // tokenUrl
);
```

* Get access token with username/password

```php
$accessToken = $quanderClient->getAccessTokenWithPassword('username', 'password');
```

* If you already have your access token

```php
$quanderClient->setAccessToken($accessToken);
```

## How to use resource managers

* Accessing your resources

```php
$attendeeManager = $test->createAttendeeManager([
    'project' => '5caea27c-79ab-4111-bafe-9568cae75e65'
]);
```

* post operation "->create(resource, options)"

```php
$attendee = new \Quander\Sdk\Model\Attendee(
    'xavier@quander.io',
    'xavier',
    'lastname'
);

$attendeeManager->create($attendee, [
    'async' => false, // If you want the qr code to be generated and retrun synchronously
])
```

## Available Resources and Operations

* Attendee(project) => post
* Account => getList, get, post // Not Implemented
* Project(account) => getList // Not Implemented
* Experience(project) => getList // Not Implemented
* Media(project) => getList // Not Implemented
* Media(attendee) => getList // Not Implemented
* Media(experience) => post // Not Implemented

## Handling Error

* Example: Handling expired token

```php
try {
    $quanderClient->setAccessToken($accessToken);
} catch (\Quander\Sdk\TokenExpiredException $exception) {
    $accessToken = $test->getRefreshedToken($accessToken);

    $quanderClient->setAccessToken($accessToken);
}
```